const path = require("path");
const webpack = require('webpack');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');

const isDevelopment = process.env.NODE_ENV !== 'production';

module.exports = {
    entry: "./src/index.js",
    output: {
        path: path.join(__dirname, "/public"),
        filename: "index-bundle.js",
        // publicPath: '/'
    },
    devtool: isDevelopment && "source-map",
    devServer: {
        port: 3000,
        open: true,
        contentBase: path.join(__dirname, "../src"),
        // host: '0.0.0.0',
        historyApiFallback: true,
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ["babel-loader"]
            },
            {
                test: /\.(scss|css)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: {
                            url: false,
                            sourceMap: true,

                        }
                    },
                    {
                        loader: "postcss-loader",
                        options: {
                            sourceMap: isDevelopment,
                            plugins: [
                                autoprefixer()
                            ]
                        },
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: isDevelopment
                        }
                    }
                ]
            },
            {
                test: /\.(jpg|png|gif|svg)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            limit: 50,
                            name: '[name].[ext]',
                            outputPath: 'static',
                        }
                    }
                ]
            },
        ]
    },
    plugins: [

        new MiniCssExtractPlugin({
            filename: "[name]-styles.css",
            chunkFilename: "[id].css",
            // publicPath: '/'
        }),

        new HtmlWebpackPlugin({
            template: "./src/index.html"
        }),

        new CopyWebpackPlugin([
            {from: 'src/static', to: 'static'}
        ]),
    ]
};
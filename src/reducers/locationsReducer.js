import {SAVE_LOCATIONS, TOGGLE_LOCATIONS, ADD_LOCATION, GET_LOCATIONS, DELETE_LOCATION} from '../actions';

const initialState = {
  locations: [],
  toggleLocations: true
};

export default function locationsReducer(state = initialState, action) {
  const {payload, type} = action;
  switch (type) {

    // case ADD_LOCATION:
    //     return {
    //         locations: [
    //             action.payload,
    //             ...state.items
    //         ]
    //     };

    case SAVE_LOCATIONS:
      return {...state, locations: state.locations.concat(payload)};

    case DELETE_LOCATION:
      let arr = [...state.locations];
      arr.pop();

      return {...state, locations: arr};

    case TOGGLE_LOCATIONS:
      return {
        ...state, toggleLocations: !action.toggleLocations};

    case GET_LOCATIONS:
      return {
        ...state
        // loading: false
      };
    default:
      return state;
  }
};
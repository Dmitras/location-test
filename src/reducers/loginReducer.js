import {SWITCH_LOGIN} from '../actions';

export default function loginReducer(state = null, action) {
  switch (action.type) {
    case SWITCH_LOGIN:
      return action.logged;
    default:
      return state;
  }
};
import {combineReducers} from 'redux';
import loginReducer from "./loginReducer";
import locationsReducer from "./locationsReducer";

export default combineReducers({
  loggedIn: loginReducer,
  locations: locationsReducer,
});
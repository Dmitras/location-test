import React, {Component} from 'react';
import {Link, NavLink} from 'react-router-dom'
import {Button, Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem} from 'reactstrap'
import {connect} from "react-redux";
import {switchLogin} from "../actions";
import styled from 'styled-components';

const SNavLink = styled(NavLink)`
  margin-right: .25em;
`;

const Navigation = (props) => {

  // state = {
  //   isOpen: false
  // };
  //
  // toggle = () => {
  //   this.setState({
  //     isOpen: !this.state.isOpen
  //   })
  // };

  return (
    <Navbar color='dark' dark expand='lg'>
      <Container>
        <NavbarBrand href='/'>
          Locations App
        </NavbarBrand>
        {/*<NavbarToggler onClick={this.toggle}/>*/}
        {/*<Collapse isOpen={this.state.isOpen} navbar>*/}
        <Nav className='mr-auto'>
          <NavItem>
            <Button onClick={() => props.switchLogin(false)}>Logout</Button>
          </NavItem>
        </Nav>
        <Nav className='ml-auto' navbar>
          <NavItem>
            <SNavLink to="/">Main</SNavLink>
          </NavItem>
          <NavItem>
            <NavLink to="/about">About</NavLink>
          </NavItem>
        </Nav>
        {/*</Collapse>*/}
      </Container>
    </Navbar>
  );
}

const mapStateToProps = state => {
  return {
    loggedIn: state.loggedIn
  };
};

const mapDispatchToProps = dispatch => {
  return {
    switchLogin: (logged) => dispatch(switchLogin(logged))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
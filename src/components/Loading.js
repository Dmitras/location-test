import React from 'react';
import { Spinner } from 'reactstrap';
import Container from "reactstrap/es/Container";
import styled from 'styled-components';

const Sh1 = styled.h1`
  margin-right: .25em;
`;

const Loading = () => {
  return (
    <Container>
      <Sh1 className='loading'>Loading</Sh1>
      <Spinner style={{width: '2rem', height: '2rem'}}/>{' '}
    </Container>
  );
};

export default Loading;

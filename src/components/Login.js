import React from 'react';
import {connect} from "react-redux";
import {Redirect} from 'react-router-dom';
import {switchLogin} from "../actions";
import {Container, Button} from "reactstrap";

const Login = (props) => {
  if (props.loggedIn) {
    const {from} = props.location.state || {from: {pathname: '/'}};
    return <Redirect to={from}/>
  }
  return (
    <Container>
      <h1>Login</h1>
      <h5>Press Login to log in</h5>
      <Button
        onClick={() => props.switchLogin(true)}
      >
        Login
      </Button>
    </Container>
  );
};

const mapStateToProps = state => {
  return {
    loggedIn: state.loggedIn
  };
};

const mapDispatchToProps = dispatch => {
  return {
    switchLogin: (logged) => dispatch(switchLogin(logged))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

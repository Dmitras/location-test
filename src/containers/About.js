import React from 'react';
import {Container, Button} from 'reactstrap';

const About = () => {
  return (
    <Container>
      <h1>About author</h1>
      <p>Hello, my name is Dmitry</p>
    </Container>
  );
};

export default About;

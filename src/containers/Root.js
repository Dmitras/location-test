import React, {Component} from 'react';
import {withRouter, Switch, Route} from "react-router-dom";
import {connect} from "react-redux";
import Loading from "../components/Loading";
import App from '../containers/App';
import Login from "../components/Login";
import {switchLogin} from "../actions";
import SecureRoute from '../components/SecureRoute';


class Root extends Component {
  componentDidMount() {
      setTimeout(() => this.props.switchLogin(true), 1500)
  }

  render() {
    if (this.props.loggedIn === null) {
      return <Loading/>
    }

    return (
      <Switch>
        <Route path='/login/' component={Login}/>
        <SecureRoute component={App}/>
      </Switch>
    );
  }
}

const mapStateToProps = state => {
  return {
    loggedIn: state.loggedIn
  };
};

const mapDispatchToProps = dispatch => {
  return {
    switchLogin: (logged) => dispatch(switchLogin(logged))
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Root))


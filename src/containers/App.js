import React from 'react';
import {connect} from 'react-redux';
import {Route, Switch, Redirect} from 'react-router-dom';
import {switchLogin} from "../actions";
import Navigation from '../components/Nav'
import About from './About'
import Main from './Main'
import '../styles/app.scss';
import {Container, Button} from "reactstrap";

const App = (props) => {

  return (
    <div>
      <Navigation/>
      <Switch>
        <Route exact path='/about' component={About}/>
        <Route exact path='/main' component={Main}/>
        <Redirect exact from='/' to='/main'/>
      </Switch>
    </div>
  );
};

export default connect(
  state => ({loggedIn: state.loggedIn}), {switchLogin})(App);


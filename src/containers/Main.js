import React, {Component} from 'react';
import Map from '../components/map/Map'
import Marker from '../components/map/Marker'
import Icon from '../components/map/Icon'
import {connect} from "react-redux";
import {switchLogin} from "../actions";
import {toggleLocations, getLocations, saveLocations, deleteLocation} from "../actions";
import PropTypes from "prop-types";
import {Container, Button} from 'reactstrap';
import DG from '2gis-maps';

class Main extends Component {
  state = {
    zoom: 13,
    center: [0, 0],
    markers: [],
    pos: [],
    draggable: true,
    withPopup: false,
    showMarkers: true,
  };

  componentWillMount(params) {
    this.setState({
      ...params,
      markers: this.props.locations
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.locations.length !== this.state.markers.length) {
      this.setState({
        markers: nextProps.locations
      });
    }
  };

  addMarker = () => {
    const {pos, draggable} = this.state;
    const {saveLocations} = this.props;
    const marker = {pos, draggable};

    saveLocations(marker);
  };


  removeMarker = () => {
    const {deleteLocation} = this.props;
    // markers.pop();
    // this.setState({
    //   markers: markers
    // });
    deleteLocation()
  };


  onToggleShowMarkers = () => {
    this.setState({
      showMarkers: !this.state.showMarkers
    });
  };

  onSaveMarkers = () => {
    const {saveLocations} = this.props;
    const {markers} = this.state;
    saveLocations(markers);
  };

  handleParams = (params) => {
    if (params) {
      this.setState({
        params,
        pos: params.latlng
      });
    }
  };

  /** Geocoder now doesn't work with new API (only paid) **/
  getObjectsOnMap(query) {
    if (this.state.params) {
      const map = this.state.params;
      // map.geocoder.get(query, {
      //   types: ['places'],
      //   limit: 100,
      //   success: function(geocoderObjects) {
      //     for(let i of geocoderObjects) {
      //       let geocoderObject = geocoderObjects[i];
      //       let markerIcon = new DG.Icon('http://api.2gis.ru/upload/images/2314.png', new DG.Size(53, 25));
      //       let marker = geocoderObject.getMarker(markerIcon, (function(geocoderObject) {
      //         return function () {
      //           let info = '';
      //
      //           info += 'Type: ' + geocoderObject.getType() + '\n';
      //           info += 'Name: ' + geocoderObject.getName() + '\n';
      //           info += 'Short name: ' + geocoderObject.getShortName() + '\n';
      //
      //           let attributes = geocoderObject.getAttributes();
      //           for (let attribute in attributes) {
      //             if (attributes.hasOwnProperty(attribute)) {
      //               info += attribute + ': ' + attributes[attribute] + '\n';
      //             }
      //           }
      //
      //           let centerGeoPoint = geocoderObject.getCenterGeoPoint();
      //           info += 'Longitude: ' + centerGeoPoint.getLon() + '\n';
      //           info += 'Latitude: ' + centerGeoPoint.getLat();
      //
      //           alert (info);
      //         }
      //       })(geocoderObject));
      //
      //       map.markers.add(marker);
      //     }
      //   },
      //
      //   failure: function(code, message) {
      //     alert(code + ' ' + message);
      //   }
      //
      // });

    }
  }

  render() {
    const {markers, showMarkers} = this.state;
    console.log('markers',markers);

    return (
      <Container>
        <h1>Map</h1>
        <Button onClick={this.onToggleShowMarkers} disabled={!this.state.markers.length}>TOGGLE SHOW
          MARKERS
        </Button>
        <Button onClick={this.onSaveMarkers}>SAVE MARKERS</Button>
        <Button onClick={this.removeMarker} disabled={!this.state.markers.length}>Remove marker</Button>
        <Map
          style={{width: "500px", height: "500px"}}
          zoom={this.state.zoom}
          pushParams={this.handleParams}
          center={this.state.center}
          onClick={this.addMarker}
        >
          {showMarkers ? markers.map(
            (marker, index) =>
              <Marker
                key={index}
                pos={marker.pos}
                draggable={marker.draggable}
              />
          ) : []}
        </Map>
        <Button onClick={this.getObjectsOnMap('gas station')}>Gas stations</Button>
        <Button onClick={this.getObjectsOnMap('pharmacies')}>Pharmacies</Button>
        <Button onClick={this.getObjectsOnMap('school')}>Schools</Button>
        <Button onClick={this.getObjectsOnMap('restaurant')}>Restaurants</Button>
      </Container>
    );
  }
}

Main.propTypes = {
  getLocations: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  locations: state.locations.locations,
});

const mapDispatchToProps = dispatch => {
  return {
    saveLocations: (markers) => dispatch(saveLocations(markers)),
    deleteLocation: (locations) => dispatch(deleteLocation(locations)),
    getLocations: (locations) => dispatch(getLocations(locations)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);
export const SWITCH_LOGIN = "SWITCH_LOGIN";
export const SAVE_LOCATIONS = "SAVE_LOCATIONS";
export const TOGGLE_LOCATIONS = "TOGGLE_LOCATIONS";
export const ADD_LOCATION = "ADD_LOCATION";
export const GET_LOCATIONS = "GET_LOCATIONS";
export const DELETE_LOCATION = "DELETE_LOCATION";


export const switchLogin = (logged) => ({
  type: SWITCH_LOGIN,
  logged: logged,
});

export const saveLocations = locations => ({

  /** There goes request **/
  // axios
  // .post('/api/items', locations)
  //     .then(res =>
  //         dispatch({
  //             type: SAVE_LOCATIONS,
  //             payload: locations
  //         })
  //     )

  type: SAVE_LOCATIONS,
  payload: locations
});

export const deleteLocation = locations => ({

  /** There goes request **/
  // axios
  // .post('/api/items', locations)
  //     .then(res =>
  //         dispatch({
  //             type: DELETE_LOCATION,
  //             payload: locations.filter(location => location._id !== action.payload)
  //         })
  //     )

  type: DELETE_LOCATION,
  payload: locations
});

export const toggleLocations = (toggleLocations) => ({
  type: TOGGLE_LOCATIONS,
  toggleLocations: true,
});

export const getLocations = (state) => ({
  type: GET_LOCATIONS,
  ...state
});

export const addLocation = (locations) => ({
  type: ADD_LOCATION,
  locations: locations
});

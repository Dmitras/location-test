import React from 'react';
import { render } from 'react-dom';
import {Provider} from 'react-redux';
import {createStore} from "redux";
import {BrowserRouter as Router} from 'react-router-dom'
import Root from "./containers/Root";
import store from "./Store";
import 'bootstrap/dist/css/bootstrap.min.css'

const target = document.getElementById('root');

render(
    <Provider store={store}>
        <Router>
            <Root/>
        </Router>
    </Provider>, target
);


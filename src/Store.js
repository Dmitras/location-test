import {createStore, applyMiddleware, compose} from "redux";
import rootReducer from './reducers';
import thunk from 'redux-thunk';
import {loadState, saveState} from './loadStorage'

const persistedState = loadState();
// const initialState = {};
const middleWare = [thunk];
const store = createStore(rootReducer, persistedState, compose(
  applyMiddleware(...middleWare),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
));

store.subscribe(() => {
  saveState({locations: store.getState().locations})
});

export default store;